#!/usr/bin/env bash

set -euo pipefail

GH_RELEASES="https://api.github.com/repos/dat-linux/releases-dconky/releases"
TOOL_NAME="dconky"
TOOL_TEST="dconky"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_github_tags() {
  curl --silent -H "Accept: application/vnd.github.v3+json" $GH_RELEASES  | grep tag_name | cut -d"\"" -f4 | sed 's/^v//'
}

list_all_versions() {
  list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  arrIN=(${version//\./ })
  ver="${arrIN[0]}.${arrIN[1]}"
  url="https://github.com/dat-linux/releases-dconky/releases/download/v${version}/dconky-${version}.tar.gz"
  #url="https://github.com/brndnmtthws/conky/releases/download/${version}/conky-x86_64.AppImage"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    chmod a+x "$install_path/bin/dconky"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
